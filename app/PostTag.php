<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $table = "user_post_tags";

    protected $fillable = ['tag_id','user_post_id','user_id'];
}
