<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\PostMedia;
use Auth;
use Storage;

class PostMediaController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request)
    {
    	$posts = PostMedia::latest();
    	if($request->get('id')){
    		$posts->where('user_post_id',$request->get('id'));
    	}
    	$posts = $posts->paginate(10);
        return view('post_images.index',compact('posts'))->with('id',$request->get('id'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('post_images.create')->with('id',$request->get('id'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'path' => 'required',
            'post_id' => 'required'
        ]);

        if($validation->errors()->all()){
            return redirect()->back()
                        ->with('errors',$validation->errors());
        }
        else{
        	$requestData = $request->all();
        	$path = $request->file('path')->store('postImages');
        	
        	unset($requestData['_token']);
        	$requestData['user_id'] = Auth::user()->id;
        	$requestData['path'] = $path;
        	$requestData['user_post_id'] = $requestData['post_id'];

        	$post = PostMedia::create($requestData);
    	    
    	    return redirect()->route('medias.index',['id' => $requestData['post_id']])
                        ->with('success','Post created successfully');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PostMedia $post)
    {
        return view('post_images.show',compact('post'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PostMedia $post)
    {
    	return view('post_images.edit',compact('post'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PostMedia $post)
    {
        $validation = Validator::make($request->all(), [
            'path' => 'required',
            'user_post_id' => 'required'
        ]);

        if($validation->errors()->all()){
            return redirect()->back()
                        ->with('errors',$validation->errors());
        }
        else{
        	$requestData = $request->all();
        	unset($requestData['_token']);
        	$post->update($requestData);
        	return redirect()->route('medias.index')
                        ->with('success','Post updated successfully');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$post = PostMedia::find($id);
        PostMedia::destroy($id);
        return redirect()->route('medias.index',['id' => $post->user_post_id])
                        ->with('success','Post deleted successfully');
    }
}
