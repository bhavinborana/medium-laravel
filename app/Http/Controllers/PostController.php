<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Post;
use App\Tag;
use App\PostTag;
use Auth;

class PostController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $posts = Post::latest()	
        						->select('user_posts.id','user_posts.post_title','user_posts.created_at')
        						->paginate(10);
        return view('posts.index',compact('posts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'post_title' => 'required',
            'static_content' => 'required'
        ]);

        if($validation->errors()->all()){
            return redirect()->back()
                        ->with('errors',$validation->errors());
        }
        else{
        	$requestData = $request->all();

        	unset($requestData['_token']);
        	$requestData['user_id'] = Auth::user()->id;

        	$post = Post::create($requestData);
    	    
    	    if(isset($requestData['tags']) && !empty($requestData['tags'])){
    	    	$tags = explode(',', $requestData['tags']);
    	    	foreach ($tags as $key => $value) {
    	    		$checkTag = Tag::firstOrCreate(['title' => $value]);
    	    		$checkTag->save();

    	    		PostTag::insert(['tag_id' => $checkTag->id, 'user_post_id' => $post->id]);
    	    	}
    	    }
    	    return redirect()->route('posts.index')
                        ->with('success','Post created successfully');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit',compact('post'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Post $post)
    {
        $validation = Validator::make($request->all(), [
            'post_title' => 'required',
            'static_content' => 'required'
        ]);

        if($validation->errors()->all()){
            return redirect()->back()
                        ->with('errors',$validation->errors());
        }
        else{
        	$requestData = $request->all();
        	unset($requestData['_token']);
        	$post->update($requestData);
        	return redirect()->route('posts.index')
                        ->with('success','Post updated successfully');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect()->route('posts.index')
                        ->with('success','Post deleted successfully');
    }
}
