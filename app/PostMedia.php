<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMedia extends Model
{
    protected $table = "user_post_images";

    protected $fillable = ['path','user_id','user_post_id'];
}
