<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "user_posts";

    protected $fillable = ['post_title','static_content','user_id'];

    public function postMedia(){
    	return $this->hasMany(PostMedia::class, 'user_post_id', 'id');
    }
}
