@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Post Images</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('medias.create',['post_id' => $id]) }}"> Upload New Image</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th width="280px">Operation</th>
        </tr>
    @foreach ($posts as $post)
    <tr>
        <td>{{ ++$i }}</td>
        <td> <img src="{{asset('storage/'.$post->path)}}" width="100"> </td>
        <td>
            {!! Form::open(['method' => 'DELETE','route' => ['medias.destroy', $post->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $posts->render() !!}
@endsection