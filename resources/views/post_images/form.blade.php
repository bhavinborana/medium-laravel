<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Title:</strong>
             {!! Form::hidden('_token', csrf_token()) !!}
             {!! Form::hidden('post_id',$id) !!}
            {!! Form::file('path', null, array('placeholder' => 'Post Title','class' => 'form-control','required' => true)) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>