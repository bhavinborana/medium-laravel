@extends('layouts.app')

@section('content')
    @if(isset($posts) && !empty($posts))
    	@foreach($posts as $key => $value)
    	<div class="panel panel-default">
        	<div class="panel-heading">{{ $value->post_title }}</div>
			<div class="panel-body">
				@if(isset($value->postMedia[0]) && !empty($value->postMedia[0]))
					<ul class="lightSlider">
						@foreach($value->postMedia as $postMedia)
						<li>
							<img src="{{asset('storage/'.$postMedia->path)}}" height="200">
						</li>
						@endforeach
					</ul>
				@endif
			</div>
			<div class="panel-body">
				{{ $value->static_content }}
			</div>
		</div>
    	@endforeach
    @else
    	No Post Found
    @endif
@endsection
