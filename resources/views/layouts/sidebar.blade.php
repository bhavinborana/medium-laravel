@if (Auth::guest())
<br>
@else
<?php $role_type = Auth::user()->role_type; ?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" id="sidebar1">
       <!--  <li class="header">HEADER</li> -->
        <!-- Optionally, you can add icons to the links -->

        <li>
           <a href="{{url('admin/dashboard')}}">
           <i class="fa fa-link"></i> <span>Dashboard</span></a>
        </li>
        @include('layouts.menu')

      </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
@endif

