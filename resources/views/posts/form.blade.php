<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Title:</strong>
             {!! Form::hidden('_token', csrf_token()) !!}
            {!! Form::text('post_title', null, array('placeholder' => 'Post Title','class' => 'form-control','required' => true)) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Post Description:</strong>
            {!! Form::textarea('static_content', null, array('class' => 'form-control','required' => true)) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Post Tags(Comma Separated):</strong>
            {!! Form::text('tags', null, array('placeholder' => 'Tags(Comma separated)','class' => 'form-control','id' => 'formTag')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>