# Medium-laravel

Laravel version 5.4

composer update

Create a database and update .env file params accordingly

then run

php artisan migrate


You can access the portal from /public

There are an option with Register and Login

You need to Register first and then you can use those credentials for Login purpose

After logged in you can add number of posts, edit, delete posts, Add images to particular posts, list and delete posts

These posts are visible at front end page. You can access that page by clicking on Medium text.
